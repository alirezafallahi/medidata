package com.medidata.billingengine.rule;

import com.medidata.billingengine.model.Patient;
import com.medidata.billingengine.service.MediHealthService;

import java.math.BigDecimal;
import java.util.function.Function;
import java.util.function.Predicate;

import static java.math.BigDecimal.ROUND_HALF_UP;

public class TotalCostCalculator {

    private static final Function<Double, Double> sixty_percent_discount_func = cost -> new BigDecimal((cost * 60) / 100).setScale(2, ROUND_HALF_UP).doubleValue();
    private static final Function<Double, Double> ninety_percent_discount_func = cost -> new BigDecimal((cost * 90) / 100).setScale(2, ROUND_HALF_UP).doubleValue();
    private static final Function<Double, Double> forty_percent_discount_func = cost -> new BigDecimal((cost * 40) / 100).setScale(2, ROUND_HALF_UP).doubleValue();
    private static final Function<Double, Double> fifteen_percent_discount_func = cost -> new BigDecimal((cost * 15) / 100).setScale(2, ROUND_HALF_UP).doubleValue();

    private static final Predicate<Patient> patient_is_between_sixtyfive_and_seventy_year_old = patient -> patient.getAge() >= 65 && patient.getAge()<= 70;
    private static final Predicate<Patient> patient_is_over_seventy_year_old = patient -> patient.getAge() > 70;
    private static final Predicate<Patient> patient_is_under_five_year_old = patient -> patient.getAge() < 5;
    private static final Predicate<Patient> patient_has_health_insurance = patient -> patient.getInsurance() != null;
    private static final Predicate<MediHealthService> is_blood_test_service = mediHealthService -> "BloodTestService".equals(mediHealthService.serviceType());

    public double calculateTotalServiceCost(Patient patient, MediHealthService mediHealthService) {
        double cost = mediHealthService.defaultCost();
        if (patient_is_between_sixtyfive_and_seventy_year_old.test(patient)) {
            cost = new BigDecimal(cost - sixty_percent_discount_func.apply(cost)).setScale(2, ROUND_HALF_UP).doubleValue();
        }
        if (patient_is_over_seventy_year_old.test(patient)) {
            cost = new BigDecimal(cost - ninety_percent_discount_func.apply(cost)).setScale(2, ROUND_HALF_UP).doubleValue();
        }
        if (patient_is_under_five_year_old.test(patient)) {
            cost = new BigDecimal(cost - forty_percent_discount_func.apply(cost)).setScale(2, ROUND_HALF_UP).doubleValue();
        }
        if (patient_has_health_insurance.test(patient)) {
            if (is_blood_test_service.test(mediHealthService)) {
                cost = new BigDecimal( cost - fifteen_percent_discount_func.apply(cost)).setScale(2, ROUND_HALF_UP).doubleValue();
            }
        }
        return cost;
    }
}
