package com.medidata.billingengine.service;

public class ECGService extends MediHealthService {

    @Override
    public double defaultCost() {
        return serviceCostRepository.ecgServiceCost();
    }

    @Override
    public String serviceType() {
        return ECGService.class.getSimpleName();
    }
}
