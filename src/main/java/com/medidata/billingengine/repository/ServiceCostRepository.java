package com.medidata.billingengine.repository;

public interface ServiceCostRepository {
    double diagnosisServiceCost();
    double xRayServiceCost();
    double bloodTestServiceCost();
    double ecgServiceCost();
    double vaccineServiceCost();
}


