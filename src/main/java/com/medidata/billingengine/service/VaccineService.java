package com.medidata.billingengine.service;

public class VaccineService extends MediHealthService {

    private static final int PER_VACCINE = 15;
    private final int numberOfVaccine;

    public VaccineService(int numberOfVaccine) {
        this.numberOfVaccine = numberOfVaccine;
    }

    @Override
    public double defaultCost() {
        return serviceCostRepository.vaccineServiceCost() + numberOfVaccine * PER_VACCINE;
    }

    @Override
    public String serviceType() {
        return VaccineService.class.getSimpleName();
    }
}
