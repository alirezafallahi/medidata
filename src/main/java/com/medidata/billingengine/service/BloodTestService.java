package com.medidata.billingengine.service;

public class BloodTestService extends MediHealthService {

    @Override
    public double defaultCost() {
        return serviceCostRepository.bloodTestServiceCost();
    }

    @Override
    public String serviceType() {
        return BloodTestService.class.getSimpleName();
    }
}
