package com.medidata.billingengine.repository;

import java.util.Properties;

public class ServiceCostFileRepositoryImpl implements ServiceCostRepository {

    private final Properties properties;

    public ServiceCostFileRepositoryImpl(Properties properties) {
        this.properties = properties;
        try {
            properties.load(ServiceCostFileRepositoryImpl.class.getResourceAsStream("../../../../cost_per_service_GBP.properties"));
        } catch (Exception e) {
            throw new RuntimeException("can not load properties file:" + e);
        }
    }

    @Override
    public double diagnosisServiceCost() {
        return Double.valueOf(properties.getProperty("Diagnosis"));
    }

    @Override
    public double xRayServiceCost() {
        return Double.valueOf(properties.getProperty("XRay"));
    }

    @Override
    public double bloodTestServiceCost() {
        return Double.valueOf(properties.getProperty("BloodTest"));
    }

    @Override
    public double ecgServiceCost() {
        return Double.valueOf(properties.getProperty("ECG"));
    }

    @Override
    public double vaccineServiceCost() {
        return Double.valueOf(properties.getProperty("Vaccine"));
    }
}
