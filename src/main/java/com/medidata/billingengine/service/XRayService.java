package com.medidata.billingengine.service;

public class XRayService extends MediHealthService {

    @Override
    public double defaultCost() {
        return serviceCostRepository.xRayServiceCost();
    }

    @Override
    public String serviceType() {
        return XRayService.class.getSimpleName();
    }
}
