package com.medidata.billingengine.service;

import com.medidata.billingengine.repository.ServiceCostFileRepositoryImpl;
import com.medidata.billingengine.repository.ServiceCostRepository;

import java.util.Properties;

public abstract class MediHealthService {

    protected final ServiceCostRepository serviceCostRepository;

    public MediHealthService() {
        this.serviceCostRepository = new ServiceCostFileRepositoryImpl(new Properties());
    }

    public abstract double defaultCost();
    public abstract String serviceType();

}
