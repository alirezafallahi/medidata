package com.medidata.billingengine.rule;

import com.medidata.billingengine.model.Insurance;
import com.medidata.billingengine.model.Patient;
import com.medidata.billingengine.service.BloodTestService;
import com.medidata.billingengine.service.DiagnosisService;
import com.medidata.billingengine.service.ECGService;
import com.medidata.billingengine.service.VaccineService;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class TotalCostCalculatorTest {

    private TotalCostCalculator underTest;

    @Before
    public void setUp() {
        underTest = new TotalCostCalculator();
    }

    @Test
    public void testGivenCitizenAgeBetween65And70WhenDiagnosisServiceUsedThenCheckReceive60PercentDiscount() {
        Patient patient = new Patient();
        patient.setAge(66);
        double totalCost = underTest.calculateTotalServiceCost(patient, new DiagnosisService());
        assertThat(totalCost, equalTo(24d));
    }

    @Test
    public void testGivenCitizenOver70WhenDiagnosisServiceUsedThenCheckReceive90PercentDiscount() {
        Patient patient = new Patient();
        patient.setAge(71);
        double totalCost = underTest.calculateTotalServiceCost(patient, new DiagnosisService());
        assertThat(totalCost, equalTo(6d));
    }

    @Test
    public void testGivenPatientUnder5WhenDiagnosisServiceUsedThenCheckReceive40PercentDiscount() {
        Patient children = new Patient();
        children.setAge(4);
        double totalCost = underTest.calculateTotalServiceCost(children, new DiagnosisService());
        assertThat(totalCost, equalTo(36d));
    }

    @Test
    public void testGivenPatientUnder5WhenDiagnosisAndBloodTestServiceUsedThenCheckReceive40PercentDiscount() {
        Patient children = new Patient();
        children.setAge(4);
        double diagnosisCost = underTest.calculateTotalServiceCost(children, new DiagnosisService());
        double bloodTestCost = underTest.calculateTotalServiceCost(children, new BloodTestService());
        assertThat(bloodTestCost + diagnosisCost, equalTo(82.8));
    }

    @Test
    public void testGivenPatientAgeBetween65And70WithInsuranceWhenBloodTestServiceUsedThenCheckReceive60PercentDiscount() {
        Patient patient = new Patient();
        patient.setAge(66);
        patient.setInsurance(new Insurance());
        double totalCost = underTest.calculateTotalServiceCost(patient, new BloodTestService());
        assertThat(totalCost, equalTo(26.52));
    }

    @Test
    public void testGivenPatientWhenOneVaccineUsedThenCheckTotalCost() {
        Patient patient = new Patient();
        patient.setAge(30);
        double totalCost = underTest.calculateTotalServiceCost(patient, new VaccineService(1));
        assertThat(totalCost, equalTo(42.5));
    }

    @Test
    public void testGivenPatientWhenTwoVaccinesUsedThenCheckTotalCost() {
        Patient patient = new Patient();
        patient.setAge(30);
        double totalCost = underTest.calculateTotalServiceCost(patient, new VaccineService(2));
        assertThat(totalCost, equalTo(57.5));
    }

    @Test
    public void testGivenPatientWhenECGServiceUsedThenCheckTotalCost() {
        Patient patient = new Patient();
        patient.setAge(20);
        double totalCost = underTest.calculateTotalServiceCost(patient, new ECGService());
        assertThat(totalCost, equalTo(200.40));
    }

}