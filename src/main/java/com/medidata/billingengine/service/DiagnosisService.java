package com.medidata.billingengine.service;

public class DiagnosisService extends MediHealthService {

    @Override
    public double defaultCost() {
        return serviceCostRepository.diagnosisServiceCost();
    }

    @Override
    public String serviceType() {
        return DiagnosisService.class.getSimpleName();
    }
}
