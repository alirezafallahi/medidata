package com.medidata.billingengine.model;

import lombok.Data;

@Data
public class Insurance {
    private String insuranceType;
}
