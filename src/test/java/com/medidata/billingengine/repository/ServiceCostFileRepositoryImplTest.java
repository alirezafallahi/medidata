package com.medidata.billingengine.repository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Properties;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ServiceCostFileRepositoryImplTest {
    private ServiceCostFileRepositoryImpl underTest;

    @Mock
    private Properties properties;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        underTest = new ServiceCostFileRepositoryImpl(properties);
    }

    @Test
    public void testWhenDiagnosisServiceCalledCheckDefaultCost() {
        when(properties.getProperty("Diagnosis")).thenReturn(String.valueOf(60));
        double totalCost = underTest.diagnosisServiceCost();
        assertThat(totalCost, equalTo(60d));
        verify(properties, times(1)).getProperty("Diagnosis");
    }

    @Test
    public void testWhenXRayServiceCalledCheckDefaultCost() {
        when(properties.getProperty("XRay")).thenReturn(String.valueOf(150));
        double totalCost = underTest.xRayServiceCost();
        assertThat(totalCost, equalTo(150d));
        verify(properties, times(1)).getProperty("XRay");
    }

    @Test
    public void testWhenBloodTestServiceCalledCheckDefaultCost() {
        when(properties.getProperty("BloodTest")).thenReturn(String.valueOf(78));
        double totalCost = underTest.bloodTestServiceCost();
        assertThat(totalCost, equalTo(78d));
        verify(properties, times(1)).getProperty("BloodTest");
    }

    @Test
    public void testWhenECGServiceCalledCheckDefaultCost() {
        when(properties.getProperty("ECG")).thenReturn(String.valueOf(200.40));
        double totalCost = underTest.ecgServiceCost();
        assertThat(totalCost, equalTo(200.40));
        verify(properties, times(1)).getProperty("ECG");
    }

    @Test
    public void testWhenVaccineServiceCalledCheckDefaultCost() {
        when(properties.getProperty("Vaccine")).thenReturn(String.valueOf(27.50));
        double totalCost = underTest.vaccineServiceCost();
        assertThat(totalCost, equalTo(27.50));
        verify(properties, times(1)).getProperty("Vaccine");
    }
}
