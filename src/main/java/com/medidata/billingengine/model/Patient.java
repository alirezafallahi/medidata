package com.medidata.billingengine.model;

import lombok.Data;

@Data
public class Patient {
    private int age;
    private Insurance insurance;
}
